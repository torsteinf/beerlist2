<html>
<head>
<title>Legg inn ny øl</title>
	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width; initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="bootstrap.css" title="main" />
	<link rel="stylesheet" type="text/css" href="bootstrap-formhelpers.min.css" title="main" />

</head>
<body>

<div class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a href="index.php" class="navbar-brand">Torsteins ølkjeller</a>
          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
          <ul class="nav navbar-nav">
            <li><a href="leggtil.php">Legg til ny øl</a></li>			
			<li><a href="updatebeer.php">Oppdater / slett øl</a></li>
            <li><a href="http://gjemmesiden.net/beer">Øl i Oslo</a></li>
          </ul>

          

        </div>
      </div>
    </div>
	
	<div id="container">


<form class="form-horizontal" name="form1" method="post" action="registrerbeer.php">
  <fieldset>
    <legend>Legg til øl</legend>
    <div class="form-group">
      <label for="inputBryggeri" class="col-lg-2 control-label">Bryggeri</label>
      <div class="col-lg-10">
        <input type="text" class="form-control" id="inputBryggeri" name="bryggeri" placeholder="Bryggeri">
      </div>
    </div>
	<div class="form-group">
      <label for="inputNavn" class="col-lg-2 control-label">Navn</label>
      <div class="col-lg-10">
        <input type="text" class="form-control" id="inputNavn" name="navn" placeholder="Navn">
      </div>
    </div>
    <div class="form-group">
      <label for="inputBeer" class="col-lg-2 control-label">Øltype</label>
      <div class="col-lg-10">
        <input type="text" class="form-control" id="inputBeer" name="beertype" placeholder="Øltype">
      </div>
    </div>
	<div class="form-group">
      <label for="inputABV" class="col-lg-2 control-label">ABV</label>
      <div class="col-lg-10">
        <input type="text" class="form-control" id="inputABV" name="abv" placeholder="ABV">
      </div>
    </div>
	<div class="form-group">
      <label for="inputML" class="col-lg-2 control-label">Antall milliliter</label>
      <div class="col-lg-10">
        <input type="text" class="form-control" id="inputML" name="ml" placeholder="Antall milliliter">
      </div>
    </div>
	<div class="form-group">
      <label for="inputBrygget" class="col-lg-2 control-label">Brygget (yyyy-mm-dd)</label>
      <div class="col-lg-10">
        <input type="text" class="form-control" id="inputBrygget" name="brygget" placeholder="Brygget (yyyy-mm-dd)">
      </div>
    </div>
	<div class="form-group">
      <label for="inputAntall" class="col-lg-2 control-label">Antall flasker</label>
      <div class="col-lg-10">
        <input type="number" class="form-control" id="inputAntall" name="antall" min="0" step="1" value="1">
      </div>
    </div>
	<div class="form-group">
      <label for="inputUTid" class="col-lg-2 control-label">Untappd-id</label>
      <div class="col-lg-10">
        <input type="text" class="form-control" id="inputUTid" name="untappdid" placeholder="Untappd-id">
      </div>
    </div>
	<div class="form-group">
      <label for="inputUTpoint" class="col-lg-2 control-label">Untappd-poeng</label>
      <div class="col-lg-10">
        <input type="text" class="form-control" id="inputUTpoint" name="untappdpoeng" placeholder="Untappd-poeng">
      </div>
    </div>
	
    
    <div class="form-group">
      <div class="col-lg-10 col-lg-offset-2">
        <button type="reset" class="btn btn-default">Cancel</button>
        <button  type="submit" name="Submit" value="Registrer" class="btn btn-primary">Registrer</button>
      </div>
    </div>
  </fieldset>
</form>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>	
<script src="bootstrap.min.js"></script>

</body>
</html>