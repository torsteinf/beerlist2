<html>
<head>
<title>Torsteins ølkjeller</title>
	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width; initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="bootstrap.css" title="main" />

</head>
<body>



<?php
include("database_connection.php");

$sql1 = "select * from Beerlist where Antall>0 group by Bryggeri, Navn;";
$resultat1=mysql_query($sql1);
$antall1=mysql_num_rows($resultat1);

$numberofbeers = mysql_result(mysql_query("select sum(Antall) from Beerlist"),0);
$numberofliters = mysql_result(mysql_query("select sum(Str) / 1000 from Beerlist"),0);

?>

<div class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a href="index.php" class="navbar-brand">Torsteins ølkjeller</a>
          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar">
          <ul class="nav navbar-nav">
            <li ><a href="leggtil.php">Legg til ny øl</a></li>
			<li><a href="updatebeer.php">Oppdater / slett øl</a></li>
            <li><a href="http://gjemmesiden.net/beer">Øl i Oslo</a></li>
          </ul>
        </div>
      </div>
    </div>
	
	<div id="container">
	
	
<?php


echo '<ul class="breadcrumb"><li class="active">Antall flasker: ', $numberofbeers, '</li>';
echo '<li class="active">Antall liter: ', $numberofliters ,'</li></ul>';

echo '<h2 id="resultat">Liste</h2><table class="table table-striped"><tr><th>Bryggeri</th><th>Navn</th><th>Type</th><th>ABV %</th><th>Milliliter</th><th class="visible-xs-block hidden-xs">Bryggedato</th><th>Antall</th><th>Untappd</th></tr>';
for($i=0; $i <$antall1; $i++)
	{
	$rad1=mysql_fetch_row($resultat1);


	echo '<tr><td>',     $rad1[1], '</td>';
	echo '<td>',     $rad1[2], '</td>';
	echo '<td>',     $rad1[3], '</td>';
	echo '<td>',     $rad1[4], '</td>';
	echo '<td>',     $rad1[5], '</td>';
	echo '<td class="visible-xs-block hidden-xs">',     $rad1[6], '</td>';
	echo '<td>',     $rad1[7], '</td>';
	echo '<td> <a href="http://untappd.com/beer/', $rad1[8], '/">', $rad1[9],'</a></td></tr>';
	}

echo '</table><br>';

?>

</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>	
<script src="bootstrap.min.js"></script>
</body>
</html>
