<html>
<head>
<title>Torsteins ølkjeller</title>
	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width; initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="bootstrap.css" title="main" />

</head>
<body>
<div class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a href="index.php" class="navbar-brand">Torsteins ølkjeller</a>
          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
          <ul class="nav navbar-nav">
            <li><a href="leggtil.php">Legg til ny øl</a></li>
			<li><a href="updatebeer.php">Oppdater / slett øl</a></li>
            <li><a href="http://gjemmesiden.net/beer">Øl i Oslo</a></li>
          </ul>

          

        </div>
      </div>
    </div>
	
	<div id="container">

<?php

include("database_connection.php");

$sql="SELECT * FROM Beerlist where Antall>0 group by Bryggeri, Navn;";
$result=mysql_query($sql);

$sql2="SELECT * FROM Beerlist where Antall=0 group by Bryggeri, Navn;";
$result2=mysql_query($sql2);

?>

<table class="table table-striped">
  <tr>
    <th colspan="11"><strong>Oppdatering og sletting av øl som er i ølkjelleren akkurat nå</strong> </th>
  </tr>

  <tr>
	<th>Bryggeri</th>
	<th>Navn</th>
	<th>Type</th>
	<th>ABV %</th>
	<th>Milliliter</th>
	<th>Antall</th>
	<th>Oppdater</th>
	<th>Slett</th>
  </tr>

  <?php
    while($rows=mysql_fetch_array($result)){
  ?>
   <tr>
    <td><?php echo $rows['Bryggeri']; ?></td>
    <td><?php echo $rows['Navn']; ?></td>
    <td><?php echo $rows['Type']; ?></td>
    <td><?php echo $rows['ABV']; ?></td> 
	<td><?php echo $rows['Str']; ?></td> 
	<td><?php echo $rows['Antall']; ?></td> 
    <td align="center"><a class="btn btn-success" href="update.php?id=<?php echo $rows['BNr']; ?>">Oppdater</a></td>
    <td align="center"><a class="btn btn-danger" href="delete.php?id=<?php echo $rows['BNr']; ?>">Slett</a></td>
  </tr>

  <?php
  }
  ?>
</table>

<br><br><br>

<table class="table table-striped">
  <tr>
    <th colspan="11"><strong>Oppdatering og sletting av øl som har vært i ølkjelleren før</strong> </th>
  </tr>

  <tr>
	<th>Bryggeri</th>
	<th>Navn</th>
	<th>Type</th>
	<th>ABV %</th>
	<th>Milliliter</th>
	<th>Antall</th>
	<th>Oppdater</th>
	<th>Slett</th>
  </tr>

  <?php
    while($rows2=mysql_fetch_array($result2)){
  ?>
   <tr>
    <td><?php echo $rows2['Bryggeri']; ?></td>
    <td><?php echo $rows2['Navn']; ?></td>
    <td><?php echo $rows2['Type']; ?></td>
    <td><?php echo $rows2['ABV']; ?></td> 
	<td><?php echo $rows2['Str']; ?></td> 
	<td><?php echo $rows2['Antall']; ?></td> 
    <td align="center"><a class="btn btn-success" href="update.php?id=<?php echo $rows2['BNr']; ?>">Oppdater</a></td>
    <td align="center"><a class="btn btn-danger" href="delete.php?id=<?php echo $rows2['BNr']; ?>">Slett</a></td>
  </tr>

  <?php
  }
  ?>
</table>

<br>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>	
<script src="bootstrap.min.js"></script>

</body>
</html>