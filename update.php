<html>
<head>
<title>Torsteins ølkjeller</title>
	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width; initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="bootstrap.css" title="main" />

</head>
<body>
<div class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a href="index.php" class="navbar-brand">Torsteins ølkjeller</a>
          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
          <ul class="nav navbar-nav">
            <li><a href="leggtil.php">Legg til ny øl</a></li>
			<li><a href="updatebeer.php">Oppdater / slett øl</a></li>
            <li><a href="http://gjemmesiden.net/beer">Øl i Oslo</a></li>
          </ul>

          

        </div>
      </div>
    </div>
	
	<div id="container">
 
 <?php 
 
include("database_connection.php");
  
  //Henter databasen for å legge inn i tekstfeltene.
  $id=$_GET['id'];

  $sql="SELECT * FROM Beerlist WHERE BNr = '$id'";
  $result=mysql_query($sql);
  $rows=mysql_fetch_array($result);
?>

<form class="form-horizontal" name="form1" method="post" action="update.inc.php">
  <fieldset>
    <legend>Oppdater ølinfo</legend>
    <div class="form-group">
      <label for="inputBryggeri" class="col-lg-2 control-label">Bryggeri</label>
      <div class="col-lg-10">
		<input class="form-control" name="bryggeri" type="text" id="inputBryggeri" value="<?php echo $rows['Bryggeri']; ?>">
      </div>
    </div>
	<div class="form-group">
      <label for="inputNavn" class="col-lg-2 control-label">Navn</label>
      <div class="col-lg-10">
		<input class="form-control" name="navn" type="text" id="inputNavn" value="<?php echo $rows['Navn']; ?>">
      </div>
    </div>
	<div class="form-group">
      <label for="inputBeertype" class="col-lg-2 control-label">Øltype</label>
      <div class="col-lg-10">
		<input class="form-control" name="type" type="text" id="inputBeertype" value="<?php echo $rows['Type']; ?>">
      </div>
    </div>
	<div class="form-group">
      <label for="inputABV" class="col-lg-2 control-label">ABV</label>
      <div class="col-lg-10">
		<input class="form-control" name="abv" type="text" id="inputABV" value="<?php echo $rows['ABV']; ?>">
      </div>
    </div>
	<div class="form-group">
      <label for="inputMl" class="col-lg-2 control-label">Milliliter</label>
      <div class="col-lg-10">
		<input class="form-control" name="milliliter" type="text" id="inputMl" value="<?php echo $rows['Str']; ?>">
      </div>
    </div>
	<div class="form-group">
      <label for="inputBrygget" class="col-lg-2 control-label">Brygget (yyyy-mm-dd)</label>
      <div class="col-lg-10">
		<input class="form-control" name="bryggedato" type="text" id="inputBrygget" value="<?php echo $rows['Brygget']; ?>">
      </div>
    </div>
	<div class="form-group">
      <label for="inputAntall" class="col-lg-2 control-label">Antall</label>
      <div class="col-lg-10">
		<input class="form-control" name="antall" type="number" min="0" id="inputAntall" value="<?php echo $rows['Antall']; ?>">
      </div>
    </div>
	<div class="form-group">
      <label for="inputUid" class="col-lg-2 control-label">Untappd-id</label>
      <div class="col-lg-10">
		<input class="form-control" name="uid" type="text" id="inputUid" value="<?php echo $rows['UntappdID']; ?>">
      </div>
    </div>
	<div class="form-group">
      <label for="inputUpoeng" class="col-lg-2 control-label">Untappd-poeng</label>
      <div class="col-lg-10">
		<input class="form-control" name="upoeng" type="text" id="inputUpoeng" value="<?php echo $rows['Untappdpoeng']; ?>">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-10 col-lg-offset-2">
        <button type="reset" class="btn btn-default">Cancel</button>		
		<input name="BNr" type="hidden" id="BNr" value="<?php echo $rows['BNr']; ?>"/>
       <a href="update.inc.php?id=<?php echo $rows['BNr']; ?>"><input type="submit" name="Submit" value="Registrer" class="btn btn-primary"/></a>
      </div>
    </div>
  </fieldset>
</form>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>	
<script src="bootstrap.min.js"></script>

</body>
</html>

